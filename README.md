# README.md

## Search Script (search.php)

### Overview

The `search.php` script is designed to facilitate searching for a specific string within a project directory. By uploading this file to the project root and accessing it through the project's base URL, users can search for a string, specify a directory path, and even filter the search by file extensions.

### Usage

1. Place the `search.php` file in the root folder of your project.
2. To search for a string, access the URL in the following format:
   ```
   BASE_URL/search.php
   ```

### Features

- **Search String:** Enter the string you want to search for.
- **Directory:** Specify the directory path to narrow down the search.
- **File Extensions:** Filter the search by providing file extensions (comma-separated).

### Example

```php
BASE_URL/search.php
```



---

## Size Calculator (size.php)

### Overview

The `size.php` script calculates the total size of files in the current directory and provides a breakdown of the size in a human-readable format (bytes, kilobytes, or megabytes).

### Usage

No specific setup is required. Simply upload the `size.php` file to the directory you want to analyze, and it will calculate and display the total size of files.

### Features

- **Recursive Calculation:** The script recursively calculates the size of files in subdirectories.
- **Human-Readable Format:** The result is presented in a human-readable format for ease of understanding.

### Example

```php
File = example.txt ==> 5.5 KB
File = another_file.php ==> 2.3 KB
Total Size: 7.8 KB
```

### Note

The displayed sizes are approximate and may vary based on the actual file sizes.

---
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search Script</title>
    <style>
        body {
            margin: 0 auto;
            width: 65%;
        }

        h1 {
            text-align: center;
            margin-top: 10px;
            text-decoration: underline;
        }

        .main-div {
            margin: 0 auto;
            float: left;
            width: 100%;
            box-shadow: 0px 1px 11px #000;
            border: 1px outset #fff;
        }

        .result-table {
            margin: 0 auto;
            float: left;
            width: 100%;
            box-shadow: 0px 1px 11px #000;
            margin-top: 5px;
            font-size: 22px;
        }

        .suggestion-string {
            clear: both;
            float: left;
            font-size: 11px;
            line-height: 26px;
            margin-left: 5px;
            position: absolute;
        }

        form {
            padding: 30px 30px 0;
        }

        .search-button {
            float: right;
            margin-top: 25px;
        }

        thead {
            text-align: center;
            font-size: 20px;
        }
    </style>
</head>
<body>
    <h1>Search Script Page</h1>
    <div class="main-div">
        <form action="" method="post">
            <table>
                <tr>
                    <td><label>Search String</label></td>
                    <td>
                        <input type="text" name="string" id="string" value="<?= $_POST['string'] ?? '' ?>">
                        <label class="suggestion-string">Enter string to search e.g. anystring</label>
                    </td>
                </tr>
                <tr>
                    <td><label>Directory</label></td>
                    <td>
                        <input type="text" name="dir" id="dir" value="<?= $_POST['dir'] ?? '' ?>">
                        <label class="suggestion-string">Enter directory path e.g. app/ and from root just add (dot) . in Directory</label>
                    </td>
                </tr>
                <tr>
                    <td><label>File Extensions</label></td>
                    <td>
                        <input type="text" name="ext" id="ext" value="<?= $_POST['ext'] ?? '' ?>">
                        <label class="suggestion-string">Enter file extensions e.g. php / For multiple file types e.g. php,phtml<br>Keep empty for all file types</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><input class="search-button" type="submit" title="Search" value="Search"></td>
                </tr>
            </table>
        </form>
    </div>
</body>
</html>

<?php
if ($_POST) {
    $string = $_POST['string'];
    $dir = $_POST['dir'];
    $extArray = ($_POST['ext'] != "") ? explode(",", $_POST['ext']) : [];

    echo "<table border='1' class='result-table'><thead><tr><td colspan='2'>Search Results</td></tr></thead><tbody><tr><td>Filepath</td><td>Last Modified Date</td></tr>";

    listFolderFiles($string, $dir, $extArray);

    echo "</tbody></table>";
}

function listFolderFiles($string, $dir, $extArray) {
    if (!$dir) {
        $dir = getcwd();
    }

    $iterator = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS),
        RecursiveIteratorIterator::SELF_FIRST
    );

    foreach ($iterator as $file) {
        $extension = $file->getExtension();

        if (empty($extArray) || in_array($extension, $extArray)) {
            $content = file_get_contents($file);
            
            if (stripos($content, $string) !== false) {
                echo "<tr><td>{$file}</td><td>" . date("F d Y H:i:s", $file->getMTime()) . "</td></tr>";
            }
        }
    }
}
?>
